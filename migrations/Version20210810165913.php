<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810165913 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cocktail ADD ingredient4_id INT DEFAULT NULL, ADD ingredient5_id INT DEFAULT NULL, ADD amount4 DOUBLE PRECISION DEFAULT NULL, ADD amount5 DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE cocktail ADD CONSTRAINT FK_7B4914D452F2D4A FOREIGN KEY (ingredient4_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE cocktail ADD CONSTRAINT FK_7B4914D4BD934A2F FOREIGN KEY (ingredient5_id) REFERENCES ingredient (id)');
        $this->addSql('CREATE INDEX IDX_7B4914D452F2D4A ON cocktail (ingredient4_id)');
        $this->addSql('CREATE INDEX IDX_7B4914D4BD934A2F ON cocktail (ingredient5_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cocktail DROP FOREIGN KEY FK_7B4914D452F2D4A');
        $this->addSql('ALTER TABLE cocktail DROP FOREIGN KEY FK_7B4914D4BD934A2F');
        $this->addSql('DROP INDEX IDX_7B4914D452F2D4A ON cocktail');
        $this->addSql('DROP INDEX IDX_7B4914D4BD934A2F ON cocktail');
        $this->addSql('ALTER TABLE cocktail DROP ingredient4_id, DROP ingredient5_id, DROP amount4, DROP amount5');
    }
}
