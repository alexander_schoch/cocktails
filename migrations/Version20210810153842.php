<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810153842 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cocktail (id INT AUTO_INCREMENT NOT NULL, ingredient1_id INT DEFAULT NULL, ingredient2_id INT DEFAULT NULL, ingredient3_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, amount1 DOUBLE PRECISION NOT NULL, amount2 DOUBLE PRECISION NOT NULL, amount3 DOUBLE PRECISION NOT NULL, INDEX IDX_7B4914D432F1DD78 (ingredient1_id), INDEX IDX_7B4914D420447296 (ingredient2_id), INDEX IDX_7B4914D498F815F3 (ingredient3_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredient (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, INDEX IDX_6BAF7870C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cocktail ADD CONSTRAINT FK_7B4914D432F1DD78 FOREIGN KEY (ingredient1_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE cocktail ADD CONSTRAINT FK_7B4914D420447296 FOREIGN KEY (ingredient2_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE cocktail ADD CONSTRAINT FK_7B4914D498F815F3 FOREIGN KEY (ingredient3_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cocktail DROP FOREIGN KEY FK_7B4914D432F1DD78');
        $this->addSql('ALTER TABLE cocktail DROP FOREIGN KEY FK_7B4914D420447296');
        $this->addSql('ALTER TABLE cocktail DROP FOREIGN KEY FK_7B4914D498F815F3');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870C54C8C93');
        $this->addSql('DROP TABLE cocktail');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE type');
    }
}
