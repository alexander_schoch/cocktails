/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/global.scss';

// start the Stimulus application
import './bootstrap';

require('bootstrap');

console.log("heyo from JavaScript");

import React from 'react';
import ReactDOM from 'react-dom';
import Alert from '@material-ui/lab/Alert';

function confirmUser(){
  console.log("confirmUser");
  var ask=confirm("Are you sure?");
  if(ask){
    window.location=target;
  }
}

export class TwintMessage extends React.Component {
  render () {
    return (
      <Alert severity="info" style={{ marginBottom: '20px' }}><b>Voluntary: </b>In order to resupply the ingredients for the cocktails, you are welcome to Twint (+41791921832) me the amount you are consuming. Thank you!</Alert>
    );
  }
}

ReactDOM.render(<TwintMessage/>, document.getElementById('twintMessage'));
