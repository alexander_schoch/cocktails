<?php

namespace App\Entity;

use App\Repository\CocktailRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CocktailRepository::class)
 */
class Cocktail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class, inversedBy="cocktails")
     */
    private $ingredient1;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit1;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient2;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit2;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient3;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit3;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient4;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit4;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient5;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit5;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient6;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount6;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit6;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient7;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount7;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit7;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient8;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount8;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit8;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient9;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount9;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit9;

    /**
     * @ORM\ManyToOne(targetEntity=IngredientType::class)
     */
    private $ingredient10;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount10;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unit10;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getIngredient1(): ?IngredientType
    {
        return $this->ingredient1;
    }

    public function setIngredient1(?IngredientType $ingredient1): self
    {
        $this->ingredient1 = $ingredient1;

        return $this;
    }

    public function getAmount1(): ?float
    {
        return $this->amount1;
    }

    public function setAmount1(?float $amount1): self
    {
        $this->amount1 = $amount1;

        return $this;
    }

    public function getUnit1(): ?string
    {
        return $this->unit1;
    }

    public function setUnit1(?string $unit1): self
    {
        $this->unit1 = $unit1;

        return $this;
    }

    public function getIngredient2(): ?IngredientType
    {
        return $this->ingredient2;
    }

    public function setIngredient2(?IngredientType $ingredient2): self
    {
        $this->ingredient2 = $ingredient2;

        return $this;
    }

    public function getAmount2(): ?float
    {
        return $this->amount2;
    }

    public function setAmount2(?float $amount2): self
    {
        $this->amount2 = $amount2;

        return $this;
    }

    public function getUnit2(): ?string
    {
        return $this->unit2;
    }

    public function setUnit2(?string $unit2): self
    {
        $this->unit2 = $unit2;

        return $this;
    }

    public function getIngredient3(): ?IngredientType
    {
        return $this->ingredient3;
    }

    public function setIngredient3(?IngredientType $ingredient3): self
    {
        $this->ingredient3 = $ingredient3;

        return $this;
    }

    public function getAmount3(): ?float
    {
        return $this->amount3;
    }

    public function setAmount3(?float $amount3): self
    {
        $this->amount3 = $amount3;

        return $this;
    }

    public function getUnit3(): ?string
    {
        return $this->unit3;
    }

    public function setUnit3(?string $unit3): self
    {
        $this->unit3 = $unit3;

        return $this;
    }

    public function getIngredient4(): ?IngredientType
    {
        return $this->ingredient4;
    }

    public function setIngredient4(?IngredientType $ingredient4): self
    {
        $this->ingredient4 = $ingredient4;

        return $this;
    }

    public function getAmount4(): ?float
    {
        return $this->amount4;
    }

    public function setAmount4(?float $amount4): self
    {
        $this->amount4 = $amount4;

        return $this;
    }

    public function getUnit4(): ?string
    {
        return $this->unit4;
    }

    public function setUnit4(?string $unit4): self
    {
        $this->unit4 = $unit4;

        return $this;
    }

    public function getIngredient5(): ?IngredientType
    {
        return $this->ingredient5;
    }

    public function setIngredient5(?IngredientType $ingredient5): self
    {
        $this->ingredient5 = $ingredient5;

        return $this;
    }

    public function getAmount5(): ?float
    {
        return $this->amount5;
    }

    public function setAmount5(?float $amount5): self
    {
        $this->amount5 = $amount5;

        return $this;
    }

    public function getUnit5(): ?string
    {
        return $this->unit5;
    }

    public function setUnit5(?string $unit5): self
    {
        $this->unit5 = $unit5;

        return $this;
    }

    public function getIngredient6(): ?IngredientType
    {
        return $this->ingredient6;
    }

    public function setIngredient6(?IngredientType $ingredient6): self
    {
        $this->ingredient6 = $ingredient6;

        return $this;
    }

    public function getAmount6(): ?float
    {
        return $this->amount6;
    }

    public function setAmount6(?float $amount6): self
    {
        $this->amount6 = $amount6;

        return $this;
    }

    public function getUnit6(): ?string
    {
        return $this->unit6;
    }

    public function setUnit6(?string $unit6): self
    {
        $this->unit6 = $unit6;

        return $this;
    }

    public function getIngredient7(): ?IngredientType
    {
        return $this->ingredient7;
    }

    public function setIngredient7(?IngredientType $ingredient7): self
    {
        $this->ingredient7 = $ingredient7;

        return $this;
    }

    public function getAmount7(): ?float
    {
        return $this->amount7;
    }

    public function setAmount7(?float $amount7): self
    {
        $this->amount7 = $amount7;

        return $this;
    }

    public function getUnit7(): ?string
    {
        return $this->unit7;
    }

    public function setUnit7(?string $unit7): self
    {
        $this->unit7 = $unit7;

        return $this;
    }

    public function getIngredient8(): ?IngredientType
    {
        return $this->ingredient8;
    }

    public function setIngredient8(?IngredientType $ingredient8): self
    {
        $this->ingredient8 = $ingredient8;

        return $this;
    }

    public function getAmount8(): ?float
    {
        return $this->amount8;
    }

    public function setAmount8(?float $amount8): self
    {
        $this->amount8 = $amount8;

        return $this;
    }

    public function getUnit8(): ?string
    {
        return $this->unit8;
    }

    public function setUnit8(?string $unit8): self
    {
        $this->unit8 = $unit8;

        return $this;
    }

    public function getIngredient9(): ?IngredientType
    {
        return $this->ingredient9;
    }

    public function setIngredient9(?IngredientType $ingredient9): self
    {
        $this->ingredient9 = $ingredient9;

        return $this;
    }

    public function getAmount9(): ?float
    {
        return $this->amount9;
    }

    public function setAmount9(?float $amount9): self
    {
        $this->amount9 = $amount9;

        return $this;
    }

    public function getUnit9(): ?string
    {
        return $this->unit9;
    }

    public function setUnit9(?string $unit9): self
    {
        $this->unit9 = $unit9;

        return $this;
    }

    public function getIngredient10(): ?IngredientType
    {
        return $this->ingredient10;
    }

    public function setIngredient10(?IngredientType $ingredient10): self
    {
        $this->ingredient10 = $ingredient10;

        return $this;
    }

    public function getAmount10(): ?float
    {
        return $this->amount10;
    }

    public function setAmount10(?float $amount10): self
    {
        $this->amount10 = $amount10;

        return $this;
    }

    public function getUnit10(): ?string
    {
        return $this->unit10;
    }

    public function setUnit10(?string $unit10): self
    {
        $this->unit10 = $unit10;

        return $this;
    }
}
