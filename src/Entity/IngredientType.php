<?php

namespace App\Entity;

use App\Repository\IngredientTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IngredientTypeRepository::class)
 */
class IngredientType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="type")
     */
    private $ingredients;

    /**
     * @ORM\OneToMany(targetEntity=Cocktail::class, mappedBy="ingredient1")
     */
    private $cocktails;

    /**
     * @ORM\OneToMany(targetEntity=Cocktail::class, mappedBy="ingredient2")
     */
    private $amount2;

    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
        $this->cocktails = new ArrayCollection();
        $this->amount2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setType($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getType() === $this) {
                $ingredient->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cocktail[]
     */
    public function getCocktails(): Collection
    {
        return $this->cocktails;
    }

    public function addCocktail(Cocktail $cocktail): self
    {
        if (!$this->cocktails->contains($cocktail)) {
            $this->cocktails[] = $cocktail;
            $cocktail->setIngredient1($this);
        }

        return $this;
    }

    public function removeCocktail(Cocktail $cocktail): self
    {
        if ($this->cocktails->removeElement($cocktail)) {
            // set the owning side to null (unless already changed)
            if ($cocktail->getIngredient1() === $this) {
                $cocktail->setIngredient1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cocktail[]
     */
    public function getAmount2(): Collection
    {
        return $this->amount2;
    }

    public function addAmount2(Cocktail $amount2): self
    {
        if (!$this->amount2->contains($amount2)) {
            $this->amount2[] = $amount2;
            $amount2->setIngredient2($this);
        }

        return $this;
    }

    public function removeAmount2(Cocktail $amount2): self
    {
        if ($this->amount2->removeElement($amount2)) {
            // set the owning side to null (unless already changed)
            if ($amount2->getIngredient2() === $this) {
                $amount2->setIngredient2(null);
            }
        }

        return $this;
    }
}
