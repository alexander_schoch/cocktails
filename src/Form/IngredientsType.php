<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\IngredientType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', TextareaType::class, [
            'label' => false, 
          ])
          ->add('isAvailable', CheckboxType::class, [
            'label' => false, 
            'required' => false,
          ])
          ->add('price', NumberType::class, [
            'label' => false, 
          ])
          ->add('isPerLiter', CheckboxType::class, [
            'label' => false, 
            'required' => false,
          ])
          ->add('type', EntityType::class, [
            'class' => IngredientType::class,
            'label' => false,
            'choice_label' => 'name',
          ])
          ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
