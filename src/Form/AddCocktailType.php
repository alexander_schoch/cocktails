<?php

namespace App\Form;

use App\Entity\Cocktail;
use App\Entity\IngredientType;
use App\Repository\IngredientTypeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddCocktailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('notes')
            ->add('ingredient1', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount1')
            ->add('unit1')
            ->add('ingredient2', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount2')
            ->add('unit2')
            ->add('ingredient3', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount3')
            ->add('unit3')
            ->add('ingredient4', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount4')
            ->add('unit4')
            ->add('ingredient5', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount5')
            ->add('unit5')
            ->add('ingredient6', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount6')
            ->add('unit6')
            ->add('ingredient7', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount7')
            ->add('unit7')
            ->add('ingredient8', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount8')
            ->add('unit8')
            ->add('ingredient9', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (IngredientTypeRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC');
              },
            ])
            ->add('amount9')
            ->add('unit9')
            ->add('ingredient10', EntityType::class, [
              'class' => IngredientType::class, 
              'choice_label' => 'name',
              'required' => false,
            ])
            ->add('amount10')
            ->add('unit10')
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cocktail::class,
        ]);
    }
}
