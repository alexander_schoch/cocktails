<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\IngredientType;
use App\Repository\IngredientTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddIngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', TextareaType::class)
          ->add('isAvailable', CheckboxType::class, [
            'required' => false,
          ])
          ->add('price', NumberType::class)
          ->add('isPerLiter', CheckboxType::class, [
            'required' => false,
          ])
          ->add('type', EntityType::class, [
            'class' => IngredientType::class,
            'choice_label' => 'name',
            'query_builder' => function (IngredientTypeRepository $er) {
              return $er->createQueryBuilder('u')
              ->orderBy('u.name', 'ASC');
            },
          ])
          ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
