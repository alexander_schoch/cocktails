<?php

namespace App\Controller;

use App\Entity\Cocktail;
use App\Form\AddCocktailType;
use App\Repository\CocktailRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(CocktailRepository $cocktailRepository): Response
    {
      $cocktails_raw = $cocktailRepository->findAll();

      $cocktails = array();

      foreach ($cocktails_raw as $index => $cocktail) {
        $ingredients = '';
        $price = 0;

        $isAvailable = true;

        for($i = 1; $i <= 10; $i++)  {
          $istring = "getIngredient" . $i;
          $astring = "getAmount" . $i;
          if(!is_null($cocktail->$istring())) {
            $ingredientType = $cocktail->$istring()->getName();
            $ingredients = $ingredients . ', ' . $ingredientType;
            $ingredient = $cocktail->$istring()->getIngredients();
            if(is_null($ingredient)) {
              $isAvailable = false;
            } else {
              $notAvailable = true;
              foreach($ingredient as $j) {
                if($j->getIsAvailable())
                  $notAvailable = false;
              }
              if($notAvailable)
                $isAvailable = false;
              //$cheapest = 0;
              //foreach($j = 0; $j < count($ingredient); $j++) {
              //  if($ingredient[$j]->isAvailable()) {
              //    if($ingredient[$j]->getPrice() < $ingredient[$cheapest])
              //      $cheapest = $j;
              //  }
              //}
            }
              //$isAvailable = false;
            //}
            if($isAvailable) {
              if($ingredient[0]->getIsPerLiter()) {
                $price += $ingredient[0]->getPrice() * $cocktail->$astring() / 100;
              } else {
                $price += $ingredient[0]->getPrice();
              }
            } else {
              $price = -1;
            }
          }
        }
        $ingredients = substr($ingredients, 2);
        array_push(
          $cocktails, 
          array(
            'name' => $cocktail->getName(),
            'ingredients' => $ingredients,
            'price' => $price, 
            'notes' => $cocktail->getNotes(), 
            'isAvailable' => $isAvailable,
            'id' => $cocktail->getId(),
          ) 
        );
      }

      $ob = new SortByName;
      $cocktails = $ob->SortByAvailability($cocktails, true);

      return $this->render('index/index.html.twig', [
        'cocktails' => $cocktails,
        'recipes' => $this->generateRecipes($cocktailRepository),
      ]);
    }

    public function generateRecipes(CocktailRepository $cocktailRepository) {
      $cocktails = $cocktailRepository->findAll();
      $recipes = array();
      foreach($cocktails as $index => $cocktail) {
        $recipelines = array();
        $containsEgg = false;
        $containsCitrus = false;
        $containsSoda = false;
        for($i = 1; $i <= 10; $i++) {
          $str1 = "getIngredient" . $i;
          $str2 = "getAmount" . $i;
          $str3 = "getUnit" . $i;
          if(!is_null($cocktail->$str1())) {
            $ing = $cocktail->$str1()->getName();
            if(in_array($ing, array("Soda", "Ginger Beer", "Mate"))) {
              $containsSoda = true;
            }
            if(in_array($ing, array("Egg White", "Egg", "Aquafaba"))) {
              $containsEgg = true;
            }
            if(in_array($ing, array("Lemon Juice", "Lime Juice"))) {
              $containsCitrus = true;
            }
            array_push(
              $recipelines, 
              $cocktail->$str2() . ' ' . $cocktail->$str3() . ' ' . $cocktail->$str1()->getName(),
            );
          }
        }

        $method = "Stir";
        if($containsSoda) {
          $method = "Stir";
        } else if ($containsEgg) {
          $method = "First a dry shake without ice, after shake with ice";
        } else if ($containsCitrus) {
          $method = "Shake";
        }
        
        array_push(
          $recipes,
          array(
            'name' => $cocktail->getName(),
            'recipelines' => $recipelines,
            'method' => $method,
            'id' => $cocktail->getId(),
          )
        );
      }
      return $recipes;
    }

    /**
     * @Route("/remove/{id}", name="removeCocktail")
     */
    public function removeCocktail(int $id, CocktailRepository $cocktailRepository) {
      $cocktail = $cocktailRepository->findById($id)[0];

      $em = $this->getDoctrine()->getManager();
      $em->remove($cocktail);
      $em->flush();

      return new RedirectResponse("/");
    }

    /**
     * @Route("/edit/{id}", name="editCocktail")
     */
    public function editCocktail(int $id, CocktailRepository $cocktailRepository, Request $request) {
      $cocktail = $cocktailRepository->findById($id)[0];

      $form = $this->createForm(AddCocktailType::class, $cocktail);

      $form->handleRequest($request);

      if($form->isSubmitted()) {
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new RedirectResponse("/");
      }

      return $this->render('edit_cocktail/index.html.twig', [
        'form' => $form->createView(),
      ]);
    }
}
