<?php

namespace App\Controller;

use App\Entity\Cocktail;
use App\Form\AddCocktailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class AddCocktailController extends AbstractController
{
    /**
     * @Route("/add", name="add_cocktail")
     */
    public function index(Request $request): Response
    {
        $cocktail = new Cocktail;  

        $form = $this->createForm(AddCocktailType::class, $cocktail);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($cocktail);
          $em->flush();

          return new RedirectResponse('/');
        }


        return $this->render('add_cocktail/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
