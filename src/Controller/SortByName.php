<?php

namespace App\Controller;

class SortByName
{
  public function SortByName(array $array, bool $isArray): array
  {
    if($isArray) {
      usort($array, array($this, "cmp")); 
    } else {
      usort($array, array($this, "cmpObject")); 
    }
    return $array;
  }

  public function cmp($a, $b)
  {
      return strcmp($a["name"], $b["name"]);
  }

  public function cmpObject($a, $b)
  {
      return strcmp($a->getName(), $b->getName());
  }

  public function SortByAvailability(array $array, bool $isArray): array
  {
    if($isArray) {
      usort($array, array($this, "cmpAv")); 
    } else {
      usort($array, array($this, "cmpAvObject")); 
    }
    return $array;
  }

  public function cmpAv($a, $b)
  {
      if($a["isAvailable"] == $b["isAvailable"]) 
        return strcmp($a["name"], $b["name"]);
      else if($a["isAvailable"])
        return -1;
      else
        return 1;
  }

  public function cmpAvObject($a, $b)
  {
    if($a->isAvailable() == $b->isAvailable()) 
      return strcmp($a->getName(), $b->getName());
    else if($a->isAvailable())
      return -1;
    else
      return 1;
  }
}
