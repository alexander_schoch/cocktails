<?php

namespace App\Controller;

use App\Entity\IngredientType;
use App\Form\AddType;
use App\Repository\IngredientTypeRepository;
use App\Repository\CocktailRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class TypeController extends AbstractController
{
    /**
     * @Route("/types", name="types")
     */
    public function index(IngredientTypeRepository $ingredientTypeRepository, CocktailRepository $cocktailRepository, Request $request): Response
    {
        $ingredientTypes = $ingredientTypeRepository->findAll();
        $cocktails = $cocktailRepository->findAll();

        $ingredientType = new IngredientType;

        $form = $this->createForm(AddType::class, $ingredientType);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($ingredientType);
          $em->flush();

          return new RedirectResponse('/types');
        }

        $istr = "getIngredient";
        $ingredientTypesArray = array();
        foreach ($ingredientTypes as $ind1 => $ingT) {
          $c = '';
          $inglist = '';
          foreach ($cocktails as $ind2 => $cocktail) {
            for($i = 1; $i <= 10; $i++) {
              $str = $istr . $i;
              $ing = $cocktail->$str();
              if(!is_null($ing)) {
                if($ing->getName() == $ingT->getName()) {
                  $c = $c . ', ' . $cocktail->getName();
                }
              }
            }
          }
          foreach ($ingT->getIngredients() as $ind3 => $ing) {
            $inglist = $inglist . ', ' . $ing->getName();
          }

          array_push(
            $ingredientTypesArray,
            array(
              'name' => $ingT->getName(),
              'containedIn' => substr($c, 2),
              'id' => $ingT->getId(),
              'ingredients' => substr($inglist, 2),
            )
          );

          $ob = new SortByName;
          $ingredientTypesArray = $ob->SortByName($ingredientTypesArray, true);
        }

        return $this->render('type/index.html.twig', [
          'form' => $form->createView(), 
          'ingredientTypes' => $ingredientTypesArray,
        ]);
    }

    /**
     * @Route("/types/edit/{id}", name="editType")
     */
    public function editType(IngredientTypeRepository $ingredientTypeRepository, int $id, Request $request): Response
    {
      $type = $ingredientTypeRepository->findById($id)[0];

      $form = $this->createForm(AddType::class, $type);

      $form->handleRequest($request);

      if($form->isSubmitted()) {
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new RedirectResponse('/types/');
      }

      return $this->render('editTypes/index.html.twig', [
        'type' => $type,
        'form' => $form->createView(),
      ]);
    }

    /**
     * @Route("/types/{id}", name="remove")
     */
    public function remove(IngredientTypeRepository $ingredientTypeRepository, int $id): Response
    {
      $ingredientType = $ingredientTypeRepository->findById($id)[0];

      $em = $this->getDoctrine()->getManager();
      $em->remove($ingredientType);
      $em->flush();

      return new RedirectResponse('/types');
    }
}
