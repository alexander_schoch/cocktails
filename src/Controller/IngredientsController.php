<?php

namespace App\Controller;

use App\Form\IngredientsType;
use App\Form\AddIngredientsType;
use App\Entity\Ingredient;
use App\Repository\IngredientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class IngredientsController extends AbstractController
{
  /**
   * @Route("/ingredients", name="ingredients")
   */
  public function index(IngredientRepository $ingredientRepository, Request $request): Response
  {
    $ingredients = $ingredientRepository->findAll();

    $ob = new SortByName;
    $ingredients = $ob->SortByName($ingredients, false);

    $ingredient = new Ingredient;

    $form = $this->createForm(AddIngredientsType::class, $ingredient);

    $form->handleRequest($request);

    if($form->isSubmitted()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($ingredient);
      $em->flush();

      return new RedirectResponse('/ingredients/');
    }

    return $this->render('ingredients/index.html.twig', [
      'ingredients' => $ingredients, 
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/ingredients/{id}", name="editIngredients")
   */
  public function editIngredient(int $id, IngredientRepository $ingredientRepository, Request $request): Response
  {
    $ingredient = $ingredientRepository->findById($id)[0];

    $form = $this->createForm(IngredientsType::class, $ingredient);

    $form->handleRequest($request);

    if($form->isSubmitted()) {
      $em = $this->getDoctrine()->getManager();
      $em->flush();

      return new RedirectResponse('/ingredients/');
    }

    return $this->render('editIngredient/index.html.twig', [
      'ingredient' => $ingredient,
      'form' => $form->createView(), 
    ]);
  }

  /**
   * @Route("/ingredients/remove/{id}", name="removeIngredients")
   */
  public function removeIngredient(int $id, IngredientRepository $ingredientRepository): Response
  {
    $ingredient = $ingredientRepository->findById($id)[0];

    $em = $this->getDoctrine()->getManager();
    $em->remove($ingredient);
    $em->flush();

    return new RedirectResponse('/ingredients/');
  }
}
